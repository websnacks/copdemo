# Community of Práctise

> Demo of Context API, testing components and Storybook

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

To spin it up open your command-line tool and run

`npm run start`

# Demos

There are 4 demos, each is in a different branch:

* `master` - normal React project with passing on props to child components
* `demo/context-api` - same project but with [Context API](https://reactjs.org/docs/context.html)
* `demo/testing` - same project as master plus some basic testing using [JEST](https://jestjs.io/) & [Enzyme](https://airbnb.io/enzyme/)
* `demo/storybook` - same project as master plus [Storybook](https://storybook.js.org/) in combination with addons:
* - [Actions](https://github.com/storybookjs/storybook/tree/master/addons/actions)
* - [Knobs](https://github.com/storybookjs/storybook/tree/master/addons/knobs)
* - [Specifications Addon](https://github.com/mthuret/storybook-addon-specifications)