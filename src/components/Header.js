import React from 'react'
import { PropTypes } from 'prop-types'

class Header extends React.Component {
    render() {
        return (
            <header>
                <h1 className="title">{this.props.branding}</h1>
                <h2 className="subtitle">{this.props.subtitle}</h2>
            </header>
        );
    }
}

Header.defaultProps = {
    branding: 'I need a title',
    subtitle: 'I need a subtitle'
};

Header.propTypes = {
    branding: PropTypes.string.isRequired,
    subtitle: PropTypes.string.isRequired
};

export default Header