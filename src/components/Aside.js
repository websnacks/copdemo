import React from 'react'
import { PropTypes } from 'prop-types'

import AsideBox from './elements/AsideBox'

class Aside extends React.Component {
    render() {
        return (
            <aside>
                <h1 className="title">Aside</h1>

                <AsideBox counter={this.props.counter} />

            </aside>
        );
    }
}

Aside.defaultProps = {
    counter: 0
};

Aside.propTypes = {
    counter: PropTypes.number.isRequired
};

export default Aside