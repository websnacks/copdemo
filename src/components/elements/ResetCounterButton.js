import React from 'react'
import { PropTypes } from 'prop-types'

class ResetNormalCounterButton extends React.Component {
    render() {
        return (
            <button className="button is-primary is-size-5" onClick={this.props.onResetHandler}>Reset counter</button>
        );
    }
}

ResetNormalCounterButton.propTypes = {
    onResetHandler: PropTypes.func.isRequired
};

export default ResetNormalCounterButton