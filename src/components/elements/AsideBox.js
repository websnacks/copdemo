import React from 'react'
import { PropTypes } from 'prop-types'

import AsideBoxCounter from './AsideBoxCounter'

class AsideBox extends React.Component {
    render() {
        return (
            <div className="box">
                <div className="is-size-5 is-italic">Update counter <strong>without</strong> Context API</div>
                <AsideBoxCounter counter={this.props.counter} />
            </div>
        );
    }
}

AsideBox.defaultProps = {
    counter: 0
};

AsideBox.propTypes = {
    counter: PropTypes.number.isRequired
};

export default AsideBox