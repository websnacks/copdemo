import React from 'react'
import { PropTypes } from 'prop-types'

class AsideBoxCounter extends React.Component {
    render() {
        return (
            <div className="is-size-5 button is-primary counter noContext_counter">{this.props.counter}</div>
        );
    }
}

AsideBoxCounter.defaultProps = {
    counter: 0
};

AsideBoxCounter.propTypes = {
    counter: PropTypes.number.isRequired
};

export default AsideBoxCounter