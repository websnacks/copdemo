import React from 'react'
import { PropTypes } from 'prop-types'

import UpdateCounterButton from './UpdateCounterButton'

class UpdateNormalBox extends React.Component {
    render() {
        return (
            <div className="box">
                <div className="is-size-5 is-italic">Update counter <strong>without</strong> Context API</div>
                <UpdateCounterButton onClickHandler={this.props.onClickHandler} />
            </div>
        );
    }
}

UpdateNormalBox.propTypes = {
    onClickHandler: PropTypes.func.isRequired
};

export default UpdateNormalBox