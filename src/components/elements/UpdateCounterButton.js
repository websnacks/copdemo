import React from 'react'
import { PropTypes } from 'prop-types'

class UpdateNormalCounterButton extends React.Component {
    render() {
        return (
            <button className="button is-primary is-size-5" onClick={this.props.onClickHandler}>Click me!</button>
        );
    }
}

UpdateNormalCounterButton.propTypes = {
    onClickHandler: PropTypes.func.isRequired
};

export default UpdateNormalCounterButton