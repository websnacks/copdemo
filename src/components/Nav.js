import React from 'react'
import { PropTypes } from 'prop-types'

import UpdateBox from './elements/UpdateBox'

class Nav extends React.Component {
    render() {
        return (
            <nav>
                <h1 className="title">Nav</h1>

                <UpdateBox onClickHandler={this.props.onClickHandler} />

            </nav>
        );
    }
}

Nav.propTypes = {
    onClickHandler: PropTypes.func.isRequired
};

export default Nav