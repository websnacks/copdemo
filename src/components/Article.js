import React from 'react'
import { PropTypes } from 'prop-types'

import ResetCounterButton from './elements/ResetCounterButton'

class Article extends React.Component {
    render() {
        return (
            <article>
                <h1 className="title">Article</h1>

                <ResetCounterButton onResetHandler={this.props.onResetHandler} />

            </article>
        );
    }
}

Article.propTypes = {
    onResetHandler: PropTypes.func.isRequired
};

export default Article