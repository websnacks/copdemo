import React from 'react'

// Components
import Header from './components/Header'
import Article from './components/Article'
import Nav from './components/Nav'
import Aside from './components/Aside'

// Bulma
import './assets/sass/App.scss'

class App extends React.PureComponent {

  constructor(props) {
    super(props);

    this.state = {
      branding: 'Community of Práctise',
      subtitle: 'Demo Context API',
      counter: 0
    };
  }

  onClickHandler = () => {
    this.setState({
      counter: this.state.counter + 1
    })
  }

  onResetHandler = () => {
    this.setState({
      counter: 0
    })
  }

  render() {
    return (
      <div className="App" >

        <Header
          branding={this.state.branding}
          subtitle={this.state.subtitle} />

        <div id="main">

          <Article onResetHandler={this.onResetHandler} />

          <Nav onClickHandler={this.onClickHandler} />

          <Aside counter={this.state.counter} />

        </div>
      </div>
    );
  }
}

export default App;